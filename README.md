# Assignment

This assignment was quite challenging for me because I didn't have any previous PHP experience, but somehow from online resources I managed it to learn what I needed.

## About Project
Project is simple eCommerce, main page is where we have product cards with information such as sku, name, price and attributes about product. There are three products DVD, Book, Furniture which have its own dimensions(MB, KG, HxWxL). I used mySQL database to save added product info and to render it on main page.

## 
I want to thank you for getting interested with my work. I am a student who started learning Front-End development months ago all by myself and I think this is the thing I want to do in my life.




